/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;


public class DrivingSubsystem extends Subsystem {


  public VictorSP leftVictor, rightVictor;
  public double lastTime, lastLeftSpeed, lastRightSpeed;
  public ADXRS450_Gyro gyro;
  public static final double MAX_ACCELERATION = 0.007;

  public DrivingSubsystem(){
    leftVictor = new VictorSP(0);
    rightVictor = new VictorSP(1);
    gyro = new ADXRS450_Gyro();
  }


  public void drive(double leftSpeed, double rightSpeed){
    lastTime = Timer.getFPGATimestamp() - lastTime;

    double deltaLeftSpeed = (leftSpeed - lastLeftSpeed)/lastTime;
    double deltaRightSpeed = (rightSpeed - lastRightSpeed)/lastTime;

    if(Math.abs(deltaLeftSpeed) > MAX_ACCELERATION){
      if(deltaLeftSpeed < 0){
        leftSpeed = -MAX_ACCELERATION * lastTime + lastLeftSpeed;
      }else{
        leftSpeed = MAX_ACCELERATION * lastTime + lastLeftSpeed;
      }
    }

    if(Math.abs(deltaRightSpeed) > MAX_ACCELERATION){
      if(deltaRightSpeed < 0){
        rightSpeed = -MAX_ACCELERATION * lastTime + lastRightSpeed;
      }else{
        rightSpeed = MAX_ACCELERATION * lastTime + lastRightSpeed;
      }
    }

    lastLeftSpeed = leftSpeed;
    lastRightSpeed = rightSpeed;

    leftVictor.set(leftSpeed);
    rightVictor.set(rightSpeed);

  }


  @Override
  public void initDefaultCommand() {
 
  }
}
